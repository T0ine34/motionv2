from json5 import loads, dumps

class Settings:
    def __init__(self, path = "config.jsonc"):
        self.path = path
        self.load()
        self.data = {}
        with open(self.path, 'r') as f:
            self.data = loads(f.read())
        
    def load(self):
        with open(self.path, 'r') as f:
            self.data = loads(f.read())
            
    def save(self):
        with open(self.path, 'w') as f:
            f.write(dumps(self.data, indent = 4))
            
    def __getitem__(self, key):
        return self.data[key]
    
    def __setitem__(self, key, value):
        self.data[key] = value
        
    def __delitem__(self, key):
        del self.data[key]
        
    def get(self, key, default = None):
        try:
            return self[key]
        except KeyError:
            self[key] = default
            print("Warning: key '%s' not found in settings, using default value '%s'" % (key, default))
            return default