from random import randint

class Color:
    
    _MAP = {
        "black": (0, 0, 0),
        "white": (255, 255, 255),
        "red": (255, 0, 0),
        "green": (0, 255, 0),
        "blue": (0, 0, 255),
        "yellow": (255, 255, 0),
        "cyan": (0, 255, 255),
        "magenta": (255, 0, 255),
        "gray": (128, 128, 128),
        "grey": (128, 128, 128),
        "maroon": (128, 0, 0),
        "olive": (128, 128, 0),
        "green": (0, 128, 0),
        "purple": (128, 0, 128),
        "teal": (0, 128, 128),
        "navy": (0, 0, 128),
        "orange": (255, 165, 0),
        "brown": (165, 42, 42),
        "pink": (255, 192, 203),
        "coral": (255, 127, 80),
        "gold": (255, 215, 0),
        "silver": (192, 192, 192),
        "crimson": (220, 20, 60),
        "lime": (0, 255, 0),
    }
    
    def __init__(self, r : int = -1, g : int = -1, b : int = -1):
        if r > 255:
            raise ValueError("r must be between 0 and 255")
        if g > 255:
            raise ValueError("g must be between 0 and 255")
        if b > 255:
            raise ValueError("b must be between 0 and 255")
        self.r = r
        self.g = g
        self.b = b
        
    def get(self):
        return (self.r, self.g, self.b)
        
    def __str__(self):
        return "color(r={}, g={}, b={})".format(self.r, self.g, self.b)
    
    def __hash__(self):
        return hash((self.r, self.g, self.b))
    
    def __eq__(self, other):
        return self.r == other.r and self.g == other.g and self.b == other.b
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __add__(self, other):
        return color(self.r + other.r, self.g + other.g, self.b + other.b)
    
    def __sub__(self, other):
        return color(self.r - other.r, self.g - other.g, self.b - other.b)
    
    def __iadd__(self, other):
        self.r += other.r
        self.g += other.g
        self.b += other.b
        return self
    
    def __isub__(self, other):
        self.r -= other.r
        self.g -= other.g
        self.b -= other.b
        return self
    
    def __copy__(self):
        return Color(self.r, self.g, self.b)
    
    last = None
    @staticmethod
    def __next__():
        result = Color.last
        Color.last.r += 128 if Color.last.r < 128 else 127
        if Color.last.r > 255:
            Color.last.r = 0
            Color.last.g += 128 if Color.last.g < 128 else 127
            if Color.last.g > 255:
                Color.last.g = 0
                Color.last.b += 128 if Color.last.b < 128 else 127
                if Color.last.b > 255:
                    Color.last.b = 0
        return result
    
    def __iter__():
        yield Color.__next__()
        
    def get_negative(self):
        return Color(255 - self.r, 255 - self.g, 255 - self.b)
    
    def hexa(self):
        return "#{:02x}{:02x}{:02x}".format(self.r, self.g, self.b)
    
    def bw(self):
        return Color(0, 0, 0) if (self.r + self.g + self.b) / 3 < 128 else Color(255, 255, 255)
    
    def greyscale(self):
        return Color((self.r + self.g + self.b) // 3, (self.r + self.g + self.b) // 3, (self.r + self.g + self.b) // 3)
    
    def invert(self):
        self.r = 255 - self.r
        self.g = 255 - self.g
        self.b = 255 - self.b
        return self
    
    def bw_invert(self):
        #return the farthest black or white color from the current one
        return Color(0, 0, 0) if (self.r + self.g + self.b) / 3 > 128 else Color(255, 255, 255)

    def bgr(self):
        return Color(self.b, self.g, self.r)
    
    @staticmethod
    def random():
        return Color(randint(0, 255), randint(0, 255), randint(0, 255))
    
    @staticmethod
    def from_hexa(s : str):
        if s[0] == "#":
            s = s[1:]
        if len(s) == 3:
            return Color(int(s[0], 16) * 17, int(s[1], 16) * 17, int(s[2], 16) * 17)
        elif len(s) == 6:
            return Color(int(s[0:2], 16), int(s[2:4], 16), int(s[4:6], 16))
        else:
            raise ValueError("Invalid hexa color string '%s'" %s)
    
    @staticmethod
    def from_string(s : str):
        if s[0] == "#":
            return Color.from_hexa(s)
        elif s in Color._MAP:
            return Color(*Color._MAP[s])  
        else:
            raise ValueError("Invalid color string '%s'" %s)

Color.last = Color(255, 0, 0)

    
if __name__ == '__main__':
    with open("colors.html", "w") as f:
        f.write("<!DOCTYPE html>\n<html>\n<head>\n<title>Colors</title>\n</head>\n<body>\n")
        first = Color.last.__copy__()
        c = first
        counter = 0
        while True:
            neg = c.bw_invert()
            f.write("<div style=\"background-color: {}; color: {};\">{}</div>\n".format(c.hexa(), neg.hexa(), str(c)))
            c = Color.__next__()
            if c == first:
                break
            counter += 1
        f.write("<p>{} colors</p>\n".format(counter))
        f.write("</body>\n</html>")