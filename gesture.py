from json5 import loads

from mouse import Mouse
import key

import points

mouse = Mouse()



def get_test(string : str):
    return getattr(points, string, None)


class Action:
    def __init__(self, button, action : str|None = None, amount : int|None = None):       
        if button in ['mouse_left', 'mouse_right', 'mouse_middle']:
            button = button[6:]
            #MOUSE click
            if action == "click":
                self.run = lambda: mouse.click(button)
            elif action == "double_click":
                self.run = lambda: mouse.double_click(button)
            elif action == "press":
                self.run = lambda: mouse.grab(button) if not mouse.grabbed else None
            elif action == "release":
                self.run = lambda: mouse.release(button) if mouse.grabbed else None
                
        elif button in ['mouse_scroll']:
            #mouse scroll
            if amount is None:
                raise Exception("mouse scroll must be defined with an amount")
            self.run = lambda: mouse.scroll(amount)
        
        else:
            if action == "click":
                self.run = lambda: key.press(button)
            elif action == "double_click":
                self.run = lambda: key.double_press(button)
            elif action == "press":
                self.run = lambda: key.hold(button)
            elif action == "release":
                self.run = lambda: key.release(button)
            else:
                raise Exception("Action must be defined")
            
        self.button = button
        self.action = action
        self.amount = amount
            
            
    def __call__(self):
        self.run()
        
    def __str__(self):
        return f"Action({self.button}{(', ' + str(self.action)) if self.action is not None else ''}{(', ' + str(self.amount)) if self.amount is not None else ''})"



class HandCondition:
    def __init__(self, string : str):
        if not string.startswith('hand is'):
            raise Exception("HandCondition must start with 'hand is'")
        tokens = string.split()[2:]
        if tokens[0] == 'not':
            self.negate = True
            tokens = tokens[1:]
        else:
            self.negate = False
        
        self.side = tokens[0]
        
    def process(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        if self.side in ['left', 'right']:
            return self.negate != (side == self.side)
        elif self.side in ['target', 'free']:
            return self.negate != (is_target if self.side == 'target' else not is_target)
        else: #it's any, so we return negate
            return self.negate
        
    def __call__(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        return self.process(points, scale, is_target, side)
    
    def _bool(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        return self.process(points, scale, is_target, side)

class Condition:
    @staticmethod
    def from_string(string : str):
        
        if string.startswith('hand'):
            return HandCondition(string)
        
        
        
        tokens = string.split()
        if len(tokens) < 3:
            raise Exception("Condition must have at least 3 tokens")

        if not (tokens[0].startswith('[') and tokens[0].endswith(']')):
            raise Exception("Point id must be surrounded by square brackets")
        point1 = int(tokens[0][1:-1])
        
        if not (tokens[2].startswith('[') and tokens[2].endswith(']')):
            raise Exception("Point id must be surrounded by square brackets")
        point2 = int(tokens[2][1:-1])
        
        tolerance = 1
        if len(tokens) == 4:
            #the last token is the tolerance, between {} brackets
            if not (tokens[3].startswith('{') and tokens[3].endswith('}')):
                raise Exception("Tolerance must be surrounded by curly brackets")
            tolerance = int(tokens[3][1:-1])
            if not 0 <= tolerance <= 4:
                raise Exception("Tolerance must be between 0 and 4")
            
            if tolerance == 0:
                tolerance = 0
            elif tolerance == 1:
                tolerance = 20
            elif tolerance == 2:
                tolerance = 50
            elif tolerance == 3:
                tolerance = 100
            elif tolerance == 4:
                tolerance = 200
        
        if not tokens[1] in ["above", "below", "far", "near", "on_the_left", "on_the_right", "vertically_aligned", "horizontally_aligned"]:
            raise Exception("Invalid condition type")
        
        return Condition(point1, point2, tolerance, tokens[1])
        
        
        
    def __init__(self, point1 : int, point2 : int, tolerance : int, condition_type : str):
        self.point1 = point1
        self.point2 = point2
        self.tolerance = tolerance
        self.condition_type = condition_type
        
    def process(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        p1 = points[self.point1]
        p2 = points[self.point2]
        
        try:
            result = get_test(self.condition_type)
            if result is None:
                raise AttributeError("Invalid condition type '%s'" % self.condition_type)
            result = result(p1, p2, self.tolerance, scale)
        except AttributeError:
            raise AttributeError("Invalid condition type '%s'" % self.condition_type)
        else:
            return result
        
    def __call__(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        return self.process(points, scale, is_target, side)
    
    def _bool(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        return self.process(points, scale, is_target, side)

    def __str__(self):
        return f"Condition({self.point1}, {self.point2}, {self.tolerance}, {self.condition_type})"



class Gesture:
    @staticmethod
    def load_gesture(path):
        with open(path, 'r') as f:
            return Gesture(loads(f.read()))
        
    def __init__(self, data):
        self.data = data
        self.label = data['label']
        self.start_action = Action(**data['start_action']) if data['start_action'] is not None else None
        self.end_action = Action(**data['end_action']) if data['end_action'] is not None else None
        self.conditions = [Condition.from_string(c) for c in data['conditions']]
        
        self.started = False
        
    def process(self, points : list[tuple[int, int]], scale : float, is_target : bool, side : str):
        return all([c.process(points, scale, is_target, side) for c in self.conditions])
    
    def __str__(self):
        return f"Gesture({self.label}, {self.start_action}, {self.end_action}, {len(self.conditions)} conditions)"
        
    def name(self) -> str:
        return self.label
        
        
    def start(self):
        self.started = True
        if self.start_action is not None:
            self.start_action()
        
    def end(self):
        self.started = False
        if self.end_action is not None:
            self.end_action()
        
        
        
        
        
def get_side(points : list[tuple[int, int]], reversed : bool = False) -> str:
    """return True if the hand is the left one, False if it's the right one"""
    if points[5][0] > points[17][0]:
        return 'left' if not reversed else 'right'
    else:
        return 'right' if not reversed else 'left'
    
def is_lefter(points1 : list[tuple[int, int]], points2 : list[tuple[int, int]]) -> bool:
    """return True if points1 is lefter than points2"""
    return points1[5][0] < points2[5][0]

def sort_hands(points1 : list[tuple[int, int]], points2 : list[tuple[int, int]]) -> tuple[list[tuple[int, int]], list[tuple[int, int]]]:
    """the fist element is the left hand, the second is the right hand"""
    if is_lefter(points1, points2):
        return points1, points2
    else:
        return points2, points1



        
if __name__ == "__main__":
    gesture = Gesture.load_gesture("fist.jsonc")
    gesture.start()