
#the distance between two points using the Manhattan distance (fastest way to compute the distance between two points)
def distance(p1: tuple[float, float], p2: tuple[float, float]) -> float:
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

def get_scale(coords):
    return distance(coords[0], coords[5])/100 #for handle the distance between the hand and the camera

def near(p1, p2, scale, tolerance, *args): #return true if we are near p2
    return distance(p1, p2) <= tolerance*scale

def far(p1, p2, scale, tolerance, *args): #return true if we are far from p2
    return distance(p1, p2) >= tolerance*scale

def above(p1, p2, *args): #return true if p1 is higher than p2
    return p1[1] < p2[1]

def below(p1, p2, *args): #return true if p1 is lower than p2
    return p1[1] > p2[1]

def on_the_left(p1, p2, *args): #return true if p1 is on the left of p2
    return p1[0] < p2[0]

def on_the_right(p1, p2, *args): #return true if p1 is on the right of p2
    return p1[0] > p2[0]

def vertically_aligned(p1, p2, scale, tolerance=60, *args): #return true if p1 and p2 are vertically aligned
    return abs(p1[0] - p2[0]) <= tolerance*scale

def horizontally_aligned(p1, p2, scale, tolerance=60, *args): #return true if p1 and p2 are horizontally aligned
    return abs(p1[1] - p2[1]) <= tolerance*scale