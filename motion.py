
############################################### libraries imported
import cv2
import mediapipe as mp
import numpy as np

import threading
import queue

from key import *

from gesture import Gesture, mouse as MOUSE, get_side, is_lefter, sort_hands
from points import get_scale

from settings import Settings

from sys import argv

from colors import Color

############################################### initialization of the mediapipe module and the camera

cap = cv2.VideoCapture(0)

if not cap.isOpened():
    cap.open()

print("camera initialized")

mpHands = mp.solutions.hands
hands = mpHands.Hands()
mpDraw = mp.solutions.drawing_utils
print("mediapipe initialized")




############################################### global variables and constants

SETTINGS = Settings()
print("settings loaded")

TARGET_HAND = SETTINGS.get("target_hand", "left") #the hand we want to track (left or right)

COLORS = (
    Color.from_string("red").bgr(),
    Color.from_string("blue").bgr()
) #the colors of the points (green for the target point, red for the other points)

QUIT = False #if true, the program will stop (used to stop all threads)

TRACKED_HAND_MARGIN = 128 #the margin around the hand we want to track (in pixels), to ensure that we can reach the edges of the screen

SCREEN_SIZE = (1920, 1080) #the size of the screen (in pixels)

HAND_POS_HISTORY = {} #the history of the position of the hand (in pixels), for detecting hand movements


OUTPUT_CAMERA_QUEUE = queue.Queue() #the queue for the camera output

CAMERA_LOCK = threading.Lock() #the lock for the camera output

TARGET_POINT = 5 if SETTINGS.get("target_point", "center") == "center" else 8 #the point we want to track (5 for the the center of the hand, 8 for the tip of the index finger)

HAND_REVERSED = SETTINGS.get("hands_reversed", False) #if true, the left hand will be the right hand and vice versa

LAST_GESTS = [None, None] #the last gestures detected (one for each hand)

############################################### initialization of the gestures

GESTURES = [Gesture.load_gesture(path) for path in SETTINGS["gestures"]]

############################################### functions

def get_draw_spec(is_target : bool):
    if is_target:
        return mpDraw.DrawingSpec(color=COLORS[0].get(), thickness=2, circle_radius=2)
    else:
        return mpDraw.DrawingSpec(color=COLORS[1].get(), thickness=2, circle_radius=2)

def going_up(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[1] < HAND_POS_HISTORY[id_hand][1]

def going_down(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[1] > HAND_POS_HISTORY[id_hand][1]

def going_left(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[0] < HAND_POS_HISTORY[id_hand][0]

def going_right(id_hand, hand_coords):
    #compare the current position of the hand with the previous one
    return hand_coords[0] > HAND_POS_HISTORY[id_hand][0]



def what_is_it(coords : list[tuple[int, int]], scale : float, is_target : bool,side : str) -> Gesture|None:
    for gesture in GESTURES:
        if gesture.process(coords, scale, is_target, side):
            return gesture
    return None

def process_frame(input):
    global LAST_GESTS
    
    #convert the image to RGB
    imageRGB = cv2.cvtColor(input, cv2.COLOR_BGR2RGB)

    #process the image for the hands detection
    results = hands.process(imageRGB)

    height, width, _ = input.shape #get the size of the image (the third value is the number of channels, which is not needed here)
    
    gest = None #the gesture that will be detected; we defined it here to avoid errors if the hand is not detected
    
    if results.multi_hand_landmarks: #if at least one hand is detected
        for id_hand, hand_landmarks in enumerate(results.multi_hand_landmarks): #for each hand detected
            handedness = results.multi_handedness[id_hand] #get the handedness of the hand (left or right)
            
            side = handedness.classification[0].label.lower() #get the side of the hand (left or right)
            
            #get the coordinates of the hand
            coords = [(int(lm.x * width), int(lm.y * height)) for lm in hand_landmarks.landmark]
            
            #is_left = get_side(coords, SETTINGS.get("hands_reversed", False)) == 'left' #check if the hand is the left one

            is_target_hand = (side == TARGET_HAND) #check if the hand is the one we want to track
            
            scale = get_scale(coords) #get the scale of the hand
            
            gest = what_is_it(coords, scale, is_target_hand, side) #get the gesture detected

            hand_coords = list(coords[TARGET_POINT]) #get the coordinates of the center of the hand

            try:
                if gest is not None and gest != LAST_GESTS[id_hand]: #if a gesture is detected and it is not the same as the last one and the cooldown is over
                    gest.start()
                    
                elif gest != LAST_GESTS[id_hand]: #if we changed gesture
                    if LAST_GESTS[id_hand] is not None:
                        LAST_GESTS[id_hand].end()
                    if gest is not None:
                        gest.start()
            except Exception as e:
                print(e)
                print(id_hand, gest, LAST_GESTS)
                exit(1)
                
                
                    
                
            LAST_GESTS[id_hand] = gest #update the last gesture detected
            
            if is_target_hand: #if the hand is the one we want to track
                
                #apply the margin to the coordinates of the hand
                hand_coords[0] = int(hand_coords[0] * (SCREEN_SIZE[0]+2*TRACKED_HAND_MARGIN) / 640)
                hand_coords[1] = int(hand_coords[1] * (SCREEN_SIZE[1]+2*TRACKED_HAND_MARGIN) / 480)

                #if the hand is out of the screen, move it back
                if hand_coords[0] < TRACKED_HAND_MARGIN:
                    hand_coords[0] = 0
                elif hand_coords[0] > SCREEN_SIZE[0] + TRACKED_HAND_MARGIN:
                    hand_coords[0] = SCREEN_SIZE[0]-1
                else:
                    hand_coords[0] -= TRACKED_HAND_MARGIN
                    
                if hand_coords[1] < TRACKED_HAND_MARGIN:
                    hand_coords[1] = 0
                elif hand_coords[1] > SCREEN_SIZE[1] + TRACKED_HAND_MARGIN:
                    hand_coords[1] = SCREEN_SIZE[1]-1
                else:
                    hand_coords[1] -= TRACKED_HAND_MARGIN
                    
                MOUSE.move(hand_coords[0], hand_coords[1]) #move the mouse to the hand
                
                
                #draw the hand
            mpDraw.draw_landmarks(input, hand_landmarks, mpHands.HAND_CONNECTIONS, get_draw_spec(is_target_hand))
            
            #set the history of the position of the hand
            HAND_POS_HISTORY[id_hand] = hand_coords
            
    else:
        if MOUSE.grabbed:
            MOUSE.release() #release the mouse if it was grabbed
            
    #add the image to the output queue
    OUTPUT_CAMERA_QUEUE.put(input)
                    







def capture_frame():
    global QUIT, CAMERA_LOCK
    try:
        while not QUIT:                
            #get the frame from the camera
            with CAMERA_LOCK:
                success, img = cap.read()
                if not success:
                    print("failed to capture frame")
                    QUIT = True
                    break
                
            #mirror the frame, because the camera is mirrored
            input = cv2.flip(img, 1)
            
            
            
            
            #start a thread that will process the frame
            th = threading.Thread(target=process_frame, args=(input,))
            th.start()
            
            
    except Exception as e:
        print("error in capture_frame:", e)
        QUIT = True
        
    finally:
        print("capture_frame stopped")
        
        


def display_image():
    global QUIT, OUTPUT_CAMERA_QUEUE
    try:
        while not QUIT:
            #get the image from the queue
            input = OUTPUT_CAMERA_QUEUE.get()
            
            #display the image
            cv2.imshow("camera", input)
            
            #wait for the user to press the escape key
            if cv2.waitKey(1) == ord('q'):
                QUIT = True
                break
            
    except Exception as e:
        print("error in display_image:", e)
        QUIT = True
        
    finally:
        print("display_image stopped")
        cv2.destroyAllWindows()
        
        
        
############################################### main

def main():
    thread = threading.Thread(target=display_image)
    thread.start()

    capture_frame()


    if MOUSE.grabbed:
        MOUSE.release()
        
    cap.release()
    cv2.destroyAllWindows()
    

def terminal():
    global QUIT
    while not QUIT:
        string = input(">>> ")
        if string == "quit" or string == "exit":
            QUIT = True
            print("goodbye")
            
        if string == "release":
            MOUSE.release()
            
            
        else:
            print("unknown command")
            
if __name__ == "__main__":
    th = threading.Thread(target=terminal, daemon=True)
    th.start()
    main()