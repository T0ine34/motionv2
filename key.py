import pyautogui as pag

def press(*key):
    # simulates a key press (if multiple keys are passed, they are pressed simultaneously)
    pag.keyDown(*key)
    pag.keyUp(*key)
    
def hold(*key):
    # holds down a key
    pag.keyDown(*key)
    
def release(*key):
    # releases a key
    pag.keyUp(*key)
    
def press_next(*key):
    #press all keys one after the other
    for k in key:
        press(k)

def double_press(*key):
    #press all keys one after the other
    press(key)
    press(key)